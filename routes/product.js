const express = require('express');
const router = express.Router();
const productController = require('../controllers/product');
const auth = require('../auth');

// Create Product (Admin Only)
router.post('/', auth.verify, (req, res) => {
	const productData = auth.decode(req.headers.authorization)
	if (productData.isAdmin == true){
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send('Not Authorized')
	}
})

// Retrieve all products
router.get('/all', (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
})

// Retrieve all active products
router.get('/', (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController))
})

// Retrieve single/specific product
router.get('/:productId', (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
})

// Update a product (Admin Only)
router.put('/:productId', auth.verify, (req, res) => {
	const productData = auth.decode(req.headers.authorization)
	if(productData.isAdmin == true){
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else{
		res.send('Not Authorized')
	}
})

// Archive a product (Admin Only)
router.put('/:productId/archive', auth.verify, (req, res) =>{
	const productData = auth.decode(req.headers.authorization)
	if(productData.isAdmin == true){
		productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send('Not Authorized')
	}
})

module.exports = router;