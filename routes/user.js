const express = require('express');
const router = express.Router();
const userController = require('../controllers/user');
const auth = require('../auth');

// Check if email exists
router.post('/checkEmail', (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
})

// Register user
router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

// Login user
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

// Retrieve all user details
router.get('/details', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.getProfile({userId: req.body.id}).then(resultFromController => res.send(resultFromController))
})

// Set user as admin (Admin Only)
router.put('/:userId/setAsAdmin', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin == false){
		userController.setAsAdmin(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else{
		res.send('False')
	}
})

// Non-admin User checkout (Create Order) Non-Admin Only
router.post('/checkout', auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization)
	let data = {
		userId: userData.id,
		productId: req.body.productId,
		quantity: req.body.quantity
	}

	if (userData.isAdmin == false){
		userController.createOrder(data).then(resultFromController => res.send(resultFromController));
	} else{
		res.send('Not Authorized')
	}
});

// Retrieve All Orders (Admin Only)
router.get('/orders', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin == true){
		userController.getAllOrders().then(resultFromController => res.send(resultFromController))
	} else{
		res.send('Not Authorized')
	}

})

// Retrieve Authenticated User's Orders (Non-Admin Only)
router.get('/myOrders', (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if (userData.isAdmin == false){
		userController.getOrder({userId: req.body.id}).then(resultFromController => res.send(resultFromController))
	} else{
		res.send('Not Authorized')
	}
})


module.exports = router;
