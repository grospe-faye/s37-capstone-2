const User = require('../models/User');
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth')

// Check if email exists
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true
		} else {
			return false
		}
	})
}

// Register user
module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

// Login user
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false
		} else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			} else{
				return false
			}
		}
	})
}

// Retrieve all user details
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	})
}

// Set user as admin (Admin Only)
module.exports.setAsAdmin = (reqParams, reqBody) => {
	let updatedAdmin = {
		isAdmin: reqBody.isAdmin
	}
	return User.findByIdAndUpdate(reqParams.userId, updatedAdmin).then((user, error) => {
		if(error){
			return false
		} else{
			return true
		}
	})
}

// Non-admin User checkout (Create Order)
module.exports.createOrder = async (data) => {

	let price = await Product.findById(data.productId).then(result => {
		return result.price
	})

	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.cart.push({
			productId: data.productId,
			quantity: data.quantity,
			totalAmount: (price*data.quantity)
		});
		return user.save().then((user, error) => {
			if(error){
				return false
			} else{
				return true
			}
		})
	})

	let isProductUpdated = await Product.findById(data.productId).then(product => {
		product.order.push({
			userId: data.userId,
			quantity: data.quantity,
			totalAmount: (price*data.quantity)
		})
		return product.save().then((product, error) => {
			if(error){
				return false
			} else{
				return true
			}
		})
	})

	if (isUserUpdated && isProductUpdated) {
		return true
	} else {
		return false
	}

}

// Retrieve All Orders (Admin Only)
module.exports.getAllOrders = () => {
	return User.find({isAdmin:false}).then(result => {
		const cart = result.map(user => {
			return{
				email: user.email,
				cart:user.cart
			}
		})
		return cart;
	})
}

// Retrieve Authenticated User's Orders (Non-Admin Only)
module.exports.getOrder = (data) => {
	return User.findById(data.userId).then(result => {
		return result.cart
	})
}

