const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({
	name: {
		type: String
	},
	description: {
		type: String
	},
	price: {
		type: Number
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	}, 
	order: [
		{
			userId: {
				type: String,
				required: [true, 'User ID is required']
			},
			quantity: {
				type: Number
			},
			totalAmount: {
				type: Number
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
	})

module.exports = mongoose.model('Product', productSchema)