const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
	email: {
		type: String
	},
	password: {
		type: String
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	cart: [
		{
			productId: {
				type: String,
				required: [true, 'User ID is required']
			},
			quantity: {
				type: Number
			},
			totalAmount: {
				type: Number
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	]

})

module.exports = mongoose.model('User', userSchema)